from dimitri.utils import *
from dimitri.model.hod_hux import HodgkinHuxley
from dimitri.model.model import Model
import matplotlib.pyplot as plt
import numpy as np
import dimitri.encoding.spike
from dimitri.encoding.rate_encoding import rate_encode
from time import time

from dimitri.model.neuron import Dendrite, Soma, Neuron, NPNeuron
from sklearn.datasets import make_blobs



#X = np.array([1, 1, 1/2, 1 , 1/2, 1/2]).reshape((BATCH_SIZE, INPUT_SIZE))
X = np.random.rand(BATCH_SIZE, INPUT_SIZE)
#y = np.array([1, 1, 0, 1, 0, 0])
X = rate_encode(X).astype(np.float32)

layer1 = NPNeuron(nb_neuron = MIDDLE_SIZE,
        input_size = INPUT_SIZE,
        first_layer = False)
layer2 = NPNeuron(nb_neuron = 2,
        input_size = MIDDLE_SIZE,
        first_layer = False)

model = Model()
model.add_layer(layer1)
model.add_layer(layer2)
tps = time()
model.calculate(X)
print("time : ", time() - tps)
hh1 = layer1.hh
hh2 = layer2.hh

title = "voltage_without_leak"
plt.figure(figsize=(16, 9))
hh1.plot_v(1, slices = (slice(None), 0, 0))
hh2.legend.append("v1")
#hh1.plot_iCurrent(start = 1, slices = (slice(None), 0, 0))
hh2.plot_v(1, slices = (slice(None), 0, 0))
hh2.plot_iCurrent(start = 1, slices = (slice(None), 0, 0))
hh2.plot(title, ylim=True, xlim = False)
