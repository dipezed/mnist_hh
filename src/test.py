from dimitri.utils import *
from dimitri.model.hod_hux import HodgkinHuxley
from dimitri.model.model import Model, Layer
import matplotlib.pyplot as plt
import numpy as np
import dimitri.encoding.spike
import dimitri.encoding.rate_encoding
from time import time 
from dimitri.model.neuron import Dendrite, Soma, Neuron, NPNeuron
from sklearn.datasets import make_blobs


INPUT_SIZE = 100
a = np.zeros((10000, BATCH_SIZE, INPUT_SIZE))
a[5000:] = 4

neurons = NPNeuron(nb_neuron = 100, input_size = INPUT_SIZE)
model = Model()
model.add_layer(neurons)

start_cpu = time()
model.calculate(a)
# End the timer
end_cpu = time()

# Calculate the CPU time used
cpu_time_used = end_cpu - start_cpu

print(f"CPU time used: {cpu_time_used} seconds")
print("best is 2.4 seconds")
