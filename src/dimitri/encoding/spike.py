import struct
import numpy as np
from dimitri.model.hod_hux import HodgkinHuxley
from dimitri.utils import *

def binary_array(array, rate = 1):
    current_shape = array.shape
    array = array.flatten()
    res = np.zeros((32 * rate, array.size))
    for i in range(array.size):
        binary_str = ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', array[i]))
        binary_list = np.array([int(bit) for bit in binary_str])
        repeater = np.repeat(binary_list, rate)
        
        res[:, i] = repeater
    res = res.reshape((32 * rate, ) + current_shape)
    res = np.transpose(res, [1, 0, 2])
    return res

def binary(num, rate = 1):
    if (isinstance(num, np.ndarray)):
        return binary_array(num, rate)

    binary_str = ''.join('{:0>8b}'.format(c) for c in struct.pack('!f', num))
    binary_list = np.array([int(bit) for bit in binary_str])
    return np.repeat(binary_list, rate)



class Spike:
    def __init__(self, start = 4.25, end = 6.7, value = 3, norm_ratio = 10):
        hh = HodgkinHuxley()
        self.start = int(start / DT)
        self.end = int(end / DT)
        self.value = value
        self.norm_ratio = norm_ratio
        for i in range(self.end):
            hh.calculate(self.value)
        self.spike = np.array(hh.v)[self.start:]

    def convert(self, spike_list: np.ndarray):
        """
        spike_list: shape (batch, time, data)
        """
        
        size = self.spike.shape[0]
        first, second, third = spike_list.shape
        test = spike_list.copy()
        #spike_list = spike_list.flatten()
        res = np.zeros((first, second * size, third))
        for i in range(first):
            for k in range(second):
                for n in range(third):
                        if (spike_list[i, k, n] == 1):
                            res[i, size * k: size * (k + 1), n] = self.spike

        #res = res.reshape((first, second * size, third))
        return res / self.norm_ratio
        

