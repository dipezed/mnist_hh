import numpy as np
from dimitri.utils import *
from dimitri.model.hod_hux import HodgkinHuxley
from time import time

from numba import jit
from numba import njit, types
from numba.core.types import float32, int32, int8

class Dendrite:
    def __init__(self):
        self.w = 1#np.abs(np.random.normal())
        self.res = 0
        self.inhibitory = np.random.uniform() < RATIO
        if (self.inhibitory):
            self.w = -self.w
    def calculate(self, I_current):
        self.res = I_current * self.w
        return self.res

class Soma:
    def __init__(self):
        pass
    def calculate(self, current_dendrites : np.ndarray):
        return np.sum(current_dendrites)

class Neuron:
    def __init__(self, nb_dendrite = None):
        self.dendrites = []
        self.soma = Soma()
        self.hh = HodgkinHuxley()
        self._launch_hh()

        if (nb_dendrite != None):
            for i in range(nb_dendrite):
                self.dendrites.append(Dendrite())

    def _launch_hh(self):
        """
        Remove the first spike
        """
        for i in range(int(50 / DT)):
            self.hh.calculate(0)
    def add_dendrite(self, dendrite):
        self.dendrites.append(dendrite)

    def calculate(self, inputs):
        """
        :param inputs: np.ndarray[shape (nb_inputs,)]
        """
        assert len(inputs) == len(self.dendrites), f"{len(inputs)}|{len(self.dendrites)}"

        current_dendrites = np.zeros((len(inputs), ))
        for i in range(len(inputs)):
            current_dendrites[i] = self.dendrites[i].calculate(inputs[i])
        i_syn = self.soma.calculate(current_dendrites)
        return self.hh.calculate(i_syn)

class NPNeuron:
    def __init__(self,
            nb_neuron = 10,
            input_size = 2,
            first_layer = False):
        self.first_layer = first_layer
        if (first_layer):
            self.w = np.ones((input_size, nb_neuron))
            self.inhibitory = []
        else:
            self.w = np.abs(np.random.uniform(low = W_LOW, high = W_HIGH, size=(input_size, nb_neuron))).astype(np.float32)
            self.inhibitory = np.random.rand(input_size, nb_neuron) < RATIO

            self.w[self.inhibitory] *= -1
            self.inhibitory = np.where(self.inhibitory, -1, 1).astype(np.int8)

            self.w_min = W_MIN * self.inhibitory
            self.w_max = W_MAX * self.inhibitory

        self.hh = HodgkinHuxley(input_size = nb_neuron)
        self.s = 0

    def calculate(self, inputs):
        """
        :param inputs: np.ndarray[shape (batch, nb_inputs)]
        """

        tps = time()
        dendrites_and_soma = inputs @ self.w # shape(batch, nb_neuron)
        res = self.hh.calculate(dendrites_and_soma) # shape(batch, nb_neuron)

        if not self.first_layer:
            """
            STDP rules
            Example:
                pre = [1, 0]; post = [0, 1, 0]
                weights_to_change = [[0, 1, 0], [0, 0, 0]]
                pre = [1, 1]; post = [0, 1, 0]
                weights_to_change = [[0, 1, 0], [0, 1, 0]]
            """
            self.w = calculate_stdp(self.w, res, inputs, self.w_min, self.w_max) 
        self.s += time() - tps
        print("time = ", self.s)
        return res


@njit([(float32[:,:],float32[:,:], float32[:,:],
    int8[:,:], int8[:,:])],
        cache=True)
def calculate_stdp(w, res, inputs, w_min, w_max):
    pos_spike = np.sum(res, axis = 0) >= VALUE_SPIKE 
    pre_spike = np.sum(inputs, axis = 0) >= VALUE_SPIKE
    pre_spike = pre_spike[:, np.newaxis]
    weights_to_change = pre_spike * pos_spike
    w_new = w + weights_to_change
    w_new = np.minimum(np.maximum(
        w_new, w_min), w_max)
    return w_new







            
