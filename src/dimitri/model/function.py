from dimitri.utils import *
from numba import njit, types
from numba.core.types import float32, int32

@njit(cache=True)
def alpha_h(v):
    return 0.07 * np.exp((-v) / 20)

@njit(cache=True)
def beta_h(v):
    return 1 / (1 + np.exp((30 - v) / 10))

@njit(cache=True)
def alpha_m(v):
    numerator = 0.1 * (25 - v)
    denominator = np.exp((25 - v) / 10) - 1
    return numerator / denominator

@njit(cache=True)
def beta_m(v):
    return 4 * np.exp((- v) / 18)

@njit(cache=True)
def alpha_n(v):
    numerator = 0.01 * (10 - v)
    denominator = np.exp((10 - v) / 10) - 1
    return numerator / denominator

@njit(cache=True)
def beta_n(v):
    return 0.125 * np.exp((-v) / 80)

@njit(cache=True)
def get_alpha_beta_mnh(v):
    alphaM = alpha_m(v - V_REST)
    betaM = beta_m(v - V_REST)

    alphaN = alpha_n(v - V_REST)
    betaN = beta_n(v - V_REST)

    alphaH = alpha_h(v - V_REST)
    betaH = beta_h(v - V_REST)
    return alphaM, betaM, alphaN, betaN, alphaH, betaH

