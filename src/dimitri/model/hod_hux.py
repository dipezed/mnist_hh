from dimitri.utils import *
from dimitri.model.function import *
import matplotlib.pyplot as plt
import numpy as np
from time import time
from numba import jit
from numba import njit, types
from numba.core.types import float32, int32

#@njit([(int32, float32[:,:,:], float32[:,:,:], float32[:,:,:],
#    float32[:,:,:], float32[:,:,:], float32[:,:,:], float32[:,:,:],
#    float32[:,:,:], float32[:,:,:], float32[:,:,:])],
#    cache=True)
def hod_hox_calculate(index, v, conductance_K, conductance_Na,
        I_Na, I_K, I_Leak, I_current,
        m, n, h):

    alphaM, betaM, alphaN, betaN, alphaH, betaH = get_alpha_beta_mnh(v[index])
    conductance_K[index + 1] = G_K * (n[index] ** 4)
    conductance_Na[index + 1] = G_NA * (m[index] ** 3) * h[index]

    I_Na[index + 1] = conductance_Na[index + 1] * (v[index] - E_NA)
    I_K[index + 1] = conductance_K[index + 1] * (v[index] - E_K)
    I_Leak[index + 1] = G_LEAK * (v[index] - E_LEAK)
    Input = I_current[index + 1] - (I_Na[index + 1] + I_K[index + 1] + I_Leak[index + 1])

    v[index + 1] = v[index] + Input * DT * (1/C)
    m[index + 1] = m[index] + (alphaM * (1 - m[index]) - betaM * m[index]) * DT
    n[index + 1] = n[index] + (alphaN * (1 - n[index]) - betaN * n[index]) * DT
    h[index + 1] = h[index] + (alphaH * (1 - h[index]) - betaH * h[index]) * DT
    

    return v[index + 1] 
class HodgkinHuxley:
    def __init__(self, batch_size=BATCH_SIZE, input_size = 2):
        self.batch_size = batch_size
        self.input_size = input_size
        self.legend = []
        self.build()
        self.time = 0

    def build(self):
        self.t = [0] 
        self.conductance_K = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.conductance_Na = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.I_current = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.I_Na = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.I_K = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.I_Leak = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.v = np.full((TIME_CIRCULAR, self.batch_size, self.input_size), V_REST, dtype=np.float32)

        alphaM, betaM, alphaN, betaN, alphaH, betaH = get_alpha_beta_mnh(self.v[0])

        self.m = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.n = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.h = np.zeros((TIME_CIRCULAR, self.batch_size, self.input_size), dtype=np.float32)
        self.m[0] = alphaM / (alphaM + betaM)
        self.n[0] = alphaN / (alphaN + betaN)
        self.h[0] = alphaH / (alphaH + betaH)

        self.index = 0
    def rotate(self):
        t = self.t[TIME_CIRCULAR//2:]
        conductance_K = self.conductance_K[TIME_CIRCULAR//2:]
        conductance_Na = self.conductance_Na[TIME_CIRCULAR//2:]
        I_current = self.I_current[TIME_CIRCULAR//2:]
        I_Na = self.I_Na[TIME_CIRCULAR//2:]
        I_K = self.I_K[TIME_CIRCULAR//2:]
        I_Leak = self.I_Leak[TIME_CIRCULAR//2:]
        v = self.v[TIME_CIRCULAR//2:]
        m = self.m[TIME_CIRCULAR//2:]
        n = self.n[TIME_CIRCULAR//2:]
        h = self.h[TIME_CIRCULAR//2:]
        self.build()
        self.t = t
        self.conductance_K[:TIME_CIRCULAR//2] = conductance_K
        self.conductance_Na[:TIME_CIRCULAR//2] = conductance_Na
        self.I_current[:TIME_CIRCULAR//2] = I_current
        self.I_Na[:TIME_CIRCULAR//2] = I_Na
        self.I_K[:TIME_CIRCULAR//2] = I_K
        self.I_Leak[:TIME_CIRCULAR//2] = I_Leak
        self.v[:TIME_CIRCULAR//2] = v
        self.n[:TIME_CIRCULAR//2] = n
        self.h[:TIME_CIRCULAR//2] = h

        self.index = TIME_CIRCULAR//2 - 1

    def calculate(self, I_current):
        if I_current.ndim == 1 and I_current.shape[0] != self.batch_size:
            raise ValueError("Input current must have the same batch size.")
        if (self.index == TIME_CIRCULAR - 1):
            self.rotate()
        self.I_current[self.index + 1] = I_current
        self.t.append(self.t[-1] + DT)
        tps = time()
        v = hod_hox_calculate(self.index, self.v, self.conductance_K,
                            self.conductance_Na, self.I_Na, self.I_K,
                            self.I_Leak, self.I_current, self.m, self.n,
                            self.h)
        self.index += 1
        res = np.zeros_like(v)
        res[v > THRESHOLD] = VALUE_SPIKE
        return res


    def __plot(self, t, v):
        plt.plot(t, v)
        plt.xlabel('time in ms')
        plt.ylabel('voltage in mv')
        plt.grid(True)
    def __plot_value(self, value, title,
            start = int(45 // DT), end = -1, slices = None):
        if end == -1:
            end = len(self.t)
        if slices == None:
            self.__plot(self.t[start:end], np.array(value[start:end])[:,0])
        else:
            self.__plot(self.t[start:end], np.array(value[start:end])[slices])
        self.legend.append(title)

    def plot_n(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.n, "n", start, end, slices)
    def plot_h(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.h, "h", start, end, slices)
    def plot_m(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.m, "m", start, end, slices)
    def plot_v(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.v, "current_v", start, end, slices)
    def plot_iCurrent(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.I_current, "I_current", start, end, slices)
    def plot_iNa(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.I_Na, "Na(Sodium)", start, end, slices)
    def plot_iK(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.I_K, "Input K(postasium)", start, end, slices)
    def plot_iLeak(self, start = int(45 // DT), end = -1, slices = None):
        self.__plot_value(self.I_Leak, "Input K(postasium)", start, end, slices)
    def plot(self, title, save = False, ylim = False, xlim = False):
        plt.title(title)
        plt.legend(self.legend)
        if ylim:
            plt.ylim(-20, 110)
        if xlim:
            plt.xlim(45, 150)
        if (save):
            plt.savefig(f"images/{title}.png")
        plt.show()
        self.legend = []
