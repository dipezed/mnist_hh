import numpy as np
from dimitri.model.hod_hux import HodgkinHuxley
from dimitri.utils import *
from dimitri.model.neuron import Neuron

class Model:
    def __init__(self):
        self.layers = []
    def add_layer(self, layer):
        self.layers.append(layer)

    def calculate(self, value):
        """
        :param value: np.ndarray -> shape (time, batch, nb_inputs)
        """
        for t in range(value.shape[0]):
            X = self.layers[0].calculate(value[t])
            for layer in self.layers[1:]:
                X = layer.calculate(X)
